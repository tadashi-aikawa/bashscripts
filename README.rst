====================================================================================================
概要
====================================================================================================

| ログ解析の利用を想定したbashスクリプトです。
| 主な用途として複雑な｢tail -f｣コマンドを簡易化するために作りました。
| 他にもsedコマンドの頻出表現を簡略化して使用できます。

====================================================================================================
動作環境
====================================================================================================

以下の環境で動作を確認しています。

::

    GNU bash, version 3.00.15(1)-release (i686-redhat-linux-gnu)
    GNU bash, version 3.2.25(1)-release (i686-redhat-linux-gnu)

多分bash以外でも動くはずですが動作確認していません。

====================================================================================================
ファイル構成
====================================================================================================

::

  + root
   - mash
   - mash.config

====================================================================================================
環境設定
====================================================================================================

環境変数PATHの通ったディレクトリに以下のファイルを配置して下さい。

--------------------------------------------------
mash
--------------------------------------------------
実行ファイルです。

--------------------------------------------------
mash.config
--------------------------------------------------
| 設定ファイルです。
| ApacheとTomcatのログ配置場所を設定して下さい。
| それぞれ、ACCESS_LOG_STORAGE と CATALINA_LOG_STORAGE です。

その後、実行権限を付与して下さい。


====================================================================================================
クイックスタート
====================================================================================================

| やりたいことと対応するコマンド例をいくつか紹介します。
| コマンドの指定順は標準入力を含めて順不同で動作します。

--------------------------------------------------
コマンド例
--------------------------------------------------

アクセスログをリアルタイムで表示する

.. sourcecode:: bash

    $ mash -ta

アクセスログを表示する

.. sourcecode:: bash

    $ mash -ca

アクセスログの末尾100行を表示する

.. sourcecode:: bash

    $ mash -ca -c 'tail -100'

アクセスログをリアルタイムで表示し、HTTPを強調表示する

.. sourcecode:: bash

    $ mash -ta -em HTTP

13:00台のアクセスログを表示する

.. sourcecode:: bash

    $ mash -ca -g 13:..:..

13:15のアクセスログをステータスコードを強調して表示する

.. sourcecode:: bash

    $ mash -ca -g 13:15:.. -em '" [0-9]\{3\}'

13:15のアクセスログをtimeを強調して表示し、最低限の情報のみを表示する

.. sourcecode:: bash

    $ mash -ca -g 13:15:.. -em time -f

アクセスログの時間のみを抽出し、時を強調する

.. sourcecode:: bash

    $ mash -ca -ex '(..:..:..)' -em ^..

アクセスログの時間のみを抽出し、時間ごとにアクセス数を調べ、｢時｣を追加する

.. sourcecode:: bash

    $ mash -ca -ex '(..):..:..' | sort | uniq -c | mash -r $ 時

カレントディレクトリのエントリサイズを表示し下3桁以内を強調する

.. sourcecode:: bash

    $ ll | awk '{print $5}' | mash -em '[0-9]\{1,3\}$'

HTTPバージョンとアクセス日時を先頭に追加抽出する

.. sourcecode:: bash

    $ mash -ca -exp '\[(.+)\]' -exp 'HTTP/([^"]+)'

====================================================================================================
基本構文
====================================================================================================

ざっくりですが･･･
詳しい例はクイックスタートを参照して下さい。

.. sourcecode:: bash

    $ mash ${対象エントリ または 各種オプション} ...


====================================================================================================
コマンドオプションの説明
====================================================================================================

.. list-table::
    :header-rows: 1

    * - オプション
      - 説明
      - 備考
    * - -ta
      - 対象エントリをアクセスログにしてtail -fする
      - tail accesslogのta
    * - -tc
      - 対象エントリをcatalina.outにしてtail -fする
      - tail catalina.outのtc
    * - -ca
      - 対象エントリをアクセスログにしてcatする
      - cat accesslogのca
    * - -cc
      - 対象エントリをcatalina.outにしてcatする
      - Catalina.outのC
    * - -f
      - アクセスログを簡易表示フォーマットにして出力する
      - formatのf
    * - -d
      - 実行せずに実行コマンドを表示する
      - dry runのd
    * - -dh
      - diff形式でsyntax highlightする
      - diff highlightのdh
    * - -c
      - コマンドを設定する
      - commandのc。-cを指定しないとデフォルト｢cat｣になる
    * - -em
      - 後述する単語を強調表示する
      - emphasesのem
    * - -ex
      - | 後述する表現を抽出する
        | ヒットしない行は表示しない
        | 必ずシングルコーテーションで括り、抽出箇所は()で括ること
        | @を表現に含む場合は \\@ とエスケープする
      - extractのex
    * - -exp
      - | 後述する単語を抽出し、各行の先頭にスペース区切りで追加出力する
        | ヒットしない行は表示しない
        | 必ずシングルコーテーションで括り、抽出箇所は()で括ること
        | @を表現に含む場合は \\@ とエスケープする
      - extract plusのexp
    * - -p
      - 指定したキーに対する値を抽出する
      - parameterのp
    * - -r
      - 後述する2単語のうち、1単語目を2単語目に置換する
      - replaceのr
    * - -ud
      - 自動判別してURLデコードする
      - url decodeのud
    * - -g
      - | 後述する単語をgrep検索する
        | -gの後にアルファベットを追加するとgrepオプションになる
        | 例えば、-giE は grep -iE と同じ
      - grepのg